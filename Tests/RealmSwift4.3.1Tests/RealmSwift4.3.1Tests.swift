import XCTest
@testable import RealmSwift4_3_1

final class RealmSwift4_3_1Tests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(RealmSwift4_3_1().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
