import XCTest

import Realm_4_3_1Tests

var tests = [XCTestCaseEntry]()
tests += Realm_4_3_1Tests.allTests()
XCTMain(tests)
